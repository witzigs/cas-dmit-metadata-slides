+++
outputs = ["Reveal"]
+++
<style>
.container{
    display: flex;
}
.col{
    flex: 1;
}
</style>

# Bibliothekarische Metadatenformate
  
---

## Intro
* Metadaten und Standards
* Daten beziehen und publizieren
* Daten analysieren
* Daten transformieren