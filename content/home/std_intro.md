+++
weight = 200
+++
## Metadatenstandards

---

### Wieso Standards?
* Verständlichkeit
* Austausch- und Nachnutzbarkeit
* Arbeitsteilung

---

### Merkmale von Standards
* Sind Modell, Muster und Autorität
* Werden entwickelt und unterhalten durch eine Community
* Haben eine Trägerorganisation, einen Aktualisierungsprozess und ein Finanzierungsmodell

---

{{% section %}}
### Kategorien von Metadatenstandards
* Datenstrukturstandards
* Datenaustauschstandards
* Dateninhaltsstandards
* Datenwertstandards

---

#### Datenstrukturstandards
* Definieren Kategorien oder Elemente
* Geben die Struktur eines Datensatz vor
* MARC, DC, EAD, ...

---

MARC21 definiert das Feld 245 für den Titel

![MARC 245](/images/marc245.png)

---

#### Datenaustauschstandards
* Definieren wie Metadaten computerlesbar abgespeichert und ausgetauscht werden
* Stehen im direkten Zusammenhang mit einem Datenstrukturstandard
* Basieren auf generell genutzten Standards
* XML (z.B.: MARCXML, Dublin Core XML Schema), RDF-Serialisierungen, ISO-2709 ("MARC Bandformat"), ...
* "Format" im alltäglichen Gebrauch wird für Datenaustausch- aber auch für Datenstrukturstandards verwendet


---

MARCXML gibt ```<record>``` als Element vor, das wiederum das Element ```<leader>``` enthalten muss und die Elemente ```<controlfield>``` und ```<datafield>``` enthalten kann

<img class="special-img-class" style="width:55%" src="/images/marc-xml.png" />

---

#### Dateninhaltsstandards
* Definieren Regeln für die Erschliessung
* Geben vor wie Metadaten zu erfassen sind
* Katalogisierungsregeln
* Können in direktem Zusammenhang mit einem Datenstrukturstandard stehen (KIDS und IDSMARC)
* RDA, AACR2, KIDS, RAK, ...

---

RDA regelt, dass der Ausgabevermerk so erfasst wird, wie er in der Informationsquelle erscheint

![RDA Ausgabevermerk](/images/rda.png)

---

#### Datenwertstandards
* Definieren Werte oder Codes
* Geben Inhalte für bestimmte Elemente vor
* Vokabulare, Thesauri, kontrollierte Listen, ...
* Werden in Dateninhalts- und Datenstrukturstandards zur Nutzung empfohlen oder vorgeschrieben
* LCSH, MeSH, GND, DDC, MARC Sprachcodes...

---

Die DDC definiert den Wert 020 für Bibliotheks- und Informationswissenschaften

![DDC 020](/images/ddc.png)

{{% /section %}}

---

### Wichtige Datenstrukturstandards im Bibliotheksbereich


