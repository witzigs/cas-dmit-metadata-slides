+++
weight = 320
+++
{{% section %}}

## SRU

---

### Intro
* **S**earch/**R**etrieval via **U**RL
* Protokoll für Suchanfragen im Internet mittels CQL (**C**ontextual **Q**uery **L**anguage)
* Spezifikationen: https://www.loc.gov/standards/sru/
* Offizielle Weiterentwicklung von Z39.50 (ab 2004)
* Version 1.1 (2004), **1.2** (2007), 2.0 (2013)

---

### Anwendungsbeispiele
* Meta-Suchportale wie [KVK](https://kvk.bibliothek.kit.edu/), [Archives Online](https://www.archives-online.org/)
* Suche im Katalog über Literaturverwaltungssysteme
* Fremddatenübernahme

---

### Grundprinzipien
* Server: stellt SRU für die Suche zur Verfügung
* Client: schickt Suchanfragen an den Server, nimmt Antworten entgegen und bereitet sie auf
* CQL für Suchanfragen
* Basiert auf XML/HTTP

---

### Response
[Beispiel](https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=searchRetrieve&recordSchema=marcxml&query=alma.subjects==architektur)
* numberOfRecords: Anzahl Treffer
* recordPosition: Position in der Trefferliste
* nextRecordPosition: nächster Treffer in der Liste

---

### [Parameter und Query](https://witzigs.gitlab.io/cas-dmit-metadata/daten_beziehen/sru/requests/)

---

### [Übung zu SRU](https://witzigs.gitlab.io/cas-dmit-metadata/daten_beziehen/sru/uebung/)

{{% /section %}}
