+++
weight = 620
+++

## Fallbeispiele

---

{{% section %}}

### Dublin Core zu MARC21

---

#### Kontext
* Ziel: Daten aus BORIS in swissbib integrieren
  * MARC21
  * Suche
  * Facetten
  * Anzeige
* Input: Daten aus BORIS in Dublin Core
* Herausforderung: Komplexeres Zielformat

---

#### Sprachangaben

* 2- oder 3-stellige Codes
* Codierung nach ISO-639-3
* MARC: Codierung nach ISO-639-2
* Anderer Datenwertstandard macht ein Mapping der Werte nötig
* Umgang mit ungültigem Wert "me"

---

#### Sprachangaben

| dc:language | zu  | MARC 008                                                      |
|-------------|-----|---------------------------------------------------------------|
| deu         |     | 201013q20202025sz    \| \| \| \| \| \|\|\|\| 00 \|**ger** d   |

---

#### Datumsangaben


| dc:date | zu  | MARC 264                      |
|---------|-----|-------------------------------|
|         |     | 264 _0 $c = Entstehungsdatum  |
|         |     | 264 _1 $c = Erscheinungsdatum |
| date    |     | 264 _2 $c = Vertriebsdatum    |
|         |     | 264 _3 $c = Herstellungsdatum |
|         |     | 264 _4 $c = Copyrightdatum    |

---

#### Datumsangaben: Ungenauigkeit
* Umwandlung von weniger strukturiertem Datenformat in feiner strukturiertes Datenformat
* Daten müssen interpretiert werden, um sie zu übertragen
* Je nach Kontext klar oder Dokumentation oder Person konsultieren
* Ungenauigkeit in Kauf nehmen

---

#### Angaben zur Zeitschrift
* MARC 773 für "Übergeordnete Ressource zu analytischen Aufnahmen"
* Unterfelder:
  * $d   Erscheinungsvermerk (NR)
  * $g   Jahresangabe, Bezeichnung des Bandes, des Hefts, der Seiten innerhalb der übergeordneten Ressource (R)
  * $t   Titel der Ressource, mit der verknüpft wird (NR)
  * $x   International Standard Serial Number  (NR)

---

#### Beispiel Feld 773
    773 0_ $t SportZeiten 
           $d Göttingen 
           $g Jg. 2, Heft 4 (2002), Seite 7-18

---

#### Beispiele ```dcterms:bibliographicCitation```
* Journal of Cellular and Molecular Medicine, 14(4), pp. 933-943. Bucharest (Romania): Wiley
* Swiss Medical Forum, 20(4748), pp. 690-694. EMH Swiss Medical Publishers
* Journal of luminescence, 222, p. 117101. Elsevier
* Nephrology, dialysis, transplantation, 5(8), pp. 630-632. Oxford University Press

---

#### Splitting


| dc:date | zu         | MARC 264                                             |
|---------|------------|------------------------------------------------------|
| $t      | ^.*, [0-9] | von Anfang bis Komma gefolgt von Leerschlag und Zahl |
| $g      |            | von dort bis zu Zahl gefolgt von Punkt               |
| $d      |            | von dort bis zum Ende                                |


[```(^.*), ([0-9].*[0-9])\.(.*)```](https://regex101.com/r/g46hW7/1)

---
#### Resultat

<img class="special-img-class" style="width:87%;border: none" src="/images/boris-swissbib-result-list.png"  alt="BORIS in swissbib"/>

---

#### Resultat

<img class="special-img-class" style="width:70%;border: none" src="/images/boris-swissbib-fields.png"  alt="BORIS in swissbib"/>

{{% /section %}}

---

{{% section %}}

### swisscollections über ArchivesOnline durchsuchen

---
#### swisscollections über ArchivesOnline durchsuchen
* Ziel: Daten aus swisscollections in ArchivesOnline integrieren
  * AO-ISAD
  * Anzeige in Trefferliste
* Input: Daten aus swisscollections in MARCXML
* Herausforderung: einfacheres Zielformat

---

#### Titel
* MARC 245 für "Titel- und Urheberangaben"
* Unterfelder:
  * $a   Haupttitel
  * $b   Titelzusatz
  * $c   Erste Verantwortlichkeitsangabe nach einem Titel
  * $p   Zählung des Werkteils/Abteilung
  * $n   Titel des Werkteils/Abteilung

---

#### Titel
* $a und $b mit " : " konkateniert in ```<isad:title>```
* $c weggelassen
* $n und $p weggelassen

---

#### Reduktion
* Für AO-ISAD verarbeitete Felder:
  * 008 oder 046
  * 245
  * 264
  * 351
  * 300
  * 856
* Konsequenz: Datenverlust

---

#### Weitere Optionen
* Prefix verwenden
* Notizfelder verwenden
* Nachteil: nur noch durch Menschen einfach interpretierbar, für maschinelle Verarbeitung Mehraufwand

---

#### Resultat

<img class="special-img-class" style="width:70%;border: none" src="/images/resultat-sc-ao.png"  alt="swisscollections in ArchivesOnline"/>


{{% /section %}}

---

{{% section %}}

### Fotosammlung auf Memobase

---

#### Fotosammlung auf Memobase
* Ziel: Bilder aus der Sammlung über Memobase verfügbar machen
* Input: Daten aus e-manuscripta (MODS/METS)
* Herausforderungen:
  * Anderes Datenmodell
  * Erschliessung der Quelldaten als Dossier

---

#### Beispiele

* [Fall mehrere Fotografien](https://www.e-manuscripta.ch/bau/content/thumbview/4710877) (über [OAI](https://www.e-manuscripta.ch/oai?verb=GetRecord&metadataPrefix=mets&&identifier=oai:www.e-manuscripta.ch:4710876))
* [Fall 1 Foto](https://www.e-manuscripta.ch/bau/content/thumbview/4635027) (über [OAI](https://www.e-manuscripta.ch/oai?verb=GetRecord&metadataPrefix=mets&&identifier=oai:www.e-manuscripta.ch:4635026))

---

#### Strukutrinformationen in METS
* Pro File: label und order
* Kontextwissen: Vor- und Rückseite gescannt
* Splitting: Jedes zweite File wird ein Dokument in Memobase

---

#### Strukturinformationen in METS
```xml
<mets:structMap TYPE="PHYSICAL">
    <mets:div TYPE="physSequence" ID="physroot">
        <mets:div ID="phys4710877" TYPE="page" LABEL="[Seite 1]"
                  CONTENTIDS="10.7891/e-manuscripta-173097?urlappend=%3fpage=4710877" ORDER="1">
            <mets:fptr FILEID="IMG_DEFAULT_4710877"/>
            <mets:fptr FILEID="IMG_THUMBS_4710877"/>
            <mets:fptr FILEID="IMG_MIN_4710877"/>
            <mets:fptr FILEID="IMG_MAX_4710877"/>
        </mets:div>
        <mets:div ID="phys4710878" TYPE="page" LABEL="[Seite 2]"
                  CONTENTIDS="10.7891/e-manuscripta-173097?urlappend=%3fpage=4710878" ORDER="2">
            <mets:fptr FILEID="IMG_DEFAULT_4710878"/>
            <mets:fptr FILEID="IMG_THUMBS_4710878"/>
            <mets:fptr FILEID="IMG_MIN_4710878"/>
            <mets:fptr FILEID="IMG_MAX_4710878"/>
        </mets:div>
        <mets:div ID="phys4710879" TYPE="page" LABEL="[Seite 3]"
                  CONTENTIDS="10.7891/e-manuscripta-173097?urlappend=%3fpage=4710879" ORDER="3">
            <mets:fptr FILEID="IMG_DEFAULT_4710879"/>
            <mets:fptr FILEID="IMG_THUMBS_4710879"/>
            <mets:fptr FILEID="IMG_MIN_4710879"/>
            <mets:fptr FILEID="IMG_MAX_4710879"/>
        </mets:div>
        <mets:div ID="phys4710880" TYPE="page" LABEL="[Seite 4]"
                  CONTENTIDS="10.7891/e-manuscripta-173097?urlappend=%3fpage=4710880" ORDER="4">
            <mets:fptr FILEID="IMG_DEFAULT_4710880"/>
            <mets:fptr FILEID="IMG_THUMBS_4710880"/>
            <mets:fptr FILEID="IMG_MIN_4710880"/>
            <mets:fptr FILEID="IMG_MAX_4710880"/>
        </mets:div>
    </mets:div>
</mets:structMap>
```

---

#### Titel
* Generischer Titel, ergänzt mit label
* tableOfContent (MARC 505): eher heikel

---

#### Fragestellungen
* Welche Informationen in die gesplittete Aufnahme übernehmen? 
  * Sollte einigermassen korrekt sein
  * Sollte möglichst viele Informationen enthalten

---

#### Resultat

<img class="special-img-class" style="width:40%;border: none" src="/images/memobase-bsp.png"  alt="Resultat Memobase"/>

{{% /section %}}

---

{{% section %}}

### Mikrofilmarchiv Musik in Aleph

---

#### Mikrofilmarchiv Musik in Aleph
* Ziel: Die Daten aus der Datenbank des Mikrofilmarchivs ins Bibliotheksverwaltungssystem migrieren
* Input: Daten aus lokaler Datenbank als Dump (CSV-Datei), kein standardisiertes Format
* Herausforderungen:
  * Datenqualität
  * Komplexeres Zielformat

---

#### Standards im Bibliothekskatalog

* Erschliessung nach RDA
* Sucheinstiege wenn möglich mit GND
* Musikalien: wenn möglich Sucheinstieg für Werk
* Musikalien: Spezifische Sacherschliessung

---

#### Autorenangaben Mikrofilmarchiv

| autor                                              | lebensdaten       |
|----------------------------------------------------|-------------------|
| Phalèse, Pierre                                    | c1510-1573        |
| Lasso / Merulo / Hassler / A. & G.  Gabrieli  u.a. |                   |
| Guglielmo Ebreo da Pesaro [Giovanni Ambrosio]      | ca.1425- nach1480 |
| Anonym                                             | um 1701           |
| Attaingnant, Pierre (Hg.)                          | ca.1494-1552      |
| Bach, Carl Philipp Emanuel                         | 1714-1788         |

---

#### Formatangaben Mikrofilmarchiv

| medium       | kategorie   | praes_form  | filmart    |
|--------------|-------------|-------------|------------|
| mikrofilm    | handschrift | St          | negativ    |
| fotokopie    | handschrift | St [7,9,12] | fotokopie  |
| giegling     | unbekannt   | Tab         | notdefined |
| mikrofilm    | druck       | Partitur    | digital    |
| digital      | handschrift | Text        | digital    |
| streifenfilm | druck       | P/St        | positiv    |

---

#### Projektablauf
* Manuelle Bereinigung der Daten in Excel
* Ergänzung von fehlenden Informationen
* Kontrolle
* Transformation Excel-Datei zu MARCXML
* Import ins Bibliotheksverwaltungssystem

----

#### Fragestellungen

* Wie gut müssen die Daten für einen Import sein?
* Was kann aufgrund der vorhandenen Daten (semi-)automatisch ergänzt werden?

---

#### [Resultat](https://ubs.swisscovery.slsp.ch/discovery/fulldisplay?docid=alma9971539160105504&context=U&vid=41SLSP_UBS:live&lang=de)

<img class="special-img-class" style="width:40%;border: none" src="/images/resultat-mfa.png"  alt="Resultat Mikrofilmarchiv"/>


{{% /section %}}

