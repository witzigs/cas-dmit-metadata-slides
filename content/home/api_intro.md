+++
weight = 300
+++
## Daten beziehen und publizieren

---

### Motivationen - wieso?
* Daten anderen Bibliotheken für Fremddatenübernahme zur Verfügung stellen
* Daten in anderen Services integrieren
* Daten als Linked Open Data publizieren
* Daten für Forschende anbieten

---

### Methoden - wie?
* Schnittstellen
* Datendump
* Service in der Suchoberfläche

---

### Datenformat - was?
* Welche Datenstrukturstandards?
* Welche Datenaustauschstandards?
* Abhängigkeit zu Quelldaten, Use Case und Methode

---

{{% section %}}

## Schnittstellen

---

### Schnittstellen
* API **A**pplication **p**rogramming **i**nterface
* Automatisierter Datenaustausch
* Anwendung/Service holt Daten für ihre Zwecke ab und/oder sendet Daten ins System zurück
* Eine Seite initialisiert Prozess:
    - Client-Server Prinzip: Server stellt Daten zur Verfügung, Client fragt Daten ab
    - WebHook: Server sendet Daten, Client empfängt

---

### Schnittstellen
* Befehl von Client wird über HTTP als Teil der Query einer URL an Server geschickt
* Der Server verarbeitet den Befehl und schickt Daten zurück
* Protokolle legen die Form der Query und der zurückgesendeten Daten fest

https://<span>www.google.<span>com/search?**hl=en&q=dogs+and+cats**

---

### Schnittstellen im Bibliotheksbereich
* OAI-PMH
* SRU
* Z39.50
* Systemspezifische Schnittstellen
* IIIF APIs

{{% /section %}}