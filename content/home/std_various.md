+++
weight = 280
draft = true

+++


#### MADS
* **M**etadata **A**uthority **D**escription **S**tandard
* Abgeleitet von MARC für Autoritätsdaten
* [MADS Official Web Site](http://www.loc.gov/standards/mads/)
* Auch als [MADS/RDF](https://id.loc.gov/ontologies/madsrdf/v1.html) publiziert

<img class="special-img-class" style="width:40%;border:none" src="/images/mads.gif" />

---

#### Linked Data Vokabulare

* Schon erwähnt: [BIBFRAME](https://id.loc.gov/ontologies/bibframe.html), [MADS/RDF](https://id.loc.gov/ontologies/madsrdf/v1.html), [DCMI Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)
* Bibliotheksbereich: [RDA Element sets](http://www.rdaregistry.info/), [GND Ontology](https://d-nb.info/standards/elementset/gnd#)
* Archivbereich: [Records in Context Ontology RiC-O](https://ica-egad.github.io/RiC-O/index.html)
* Weitere:  [Wikidata](https://www.wikidata.org/wiki/Wikidata:List_of_properties), [FOAF](http://xmlns.com/foaf/0.1/), [Schema.org](https://schema.org/), [PROV-O](https://www.w3.org/TR/prov-o/), [PREMIS](http://www.loc.gov/standards/premis/ontology/index.html), [CIDOC CRM](http://www.cidoc-crm.org/), [SKOS](https://www.w3.org/2009/08/skos-reference/skos.html), ...