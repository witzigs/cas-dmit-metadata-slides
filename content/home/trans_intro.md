+++
weight = 600
draft = true
+++

# Daten transformieren

---

## Ziele

* Daten verbessern (ergänzen, einem neuen Standard anpassen, korrigieren, ...)
* Daten in verschiedenen Formen zur Verfügung stellen (Internformat vs. Austauschformat, LOD-Kontext)
* Daten aus verschiedenen Quellen homogenisieren (Aggregator, Suche ermöglichen)
* Daten indexieren (Dokument für Suchmaschine, Text Analysis, ...)
* Daten für Weiterverarbeitung und Analyse aufbereiten (Zeilenbasierte Formate, CSV, ...)

---

## Hilfsmittel
* Crosswalks zwischen Formaten
* Zur Verfügung gestellte Skripte
* Diverse [Tools](https://witzigs.gitlab.io/cas-dmit-metadata-docs/links/tools/)

---

{{% section %}}

## Herausforderungen

---

### Verlust an Genauigkeit
* Bei Umwandlung von stark strukturierten Daten in ein weniger strukturiertes Format
* Worst case: Datenverlust
* Normalerweise steht mindestens ein Notizfeld zur Verfügung, das mit Prefix verwendet werden kann. Grosser Nachteil: nur noch durch Menschen interpretierbar

---

| MODS host, related item ([Beispiel Serval](https://serval.unil.ch/oaiprovider?verb=GetRecord&metadataPrefix=mods&identifier=oai:serval.unil.ch:BIB_35B084C09815))  | MARC 773 ([Beispiel swissbib](https://legacy.swissbib.ch/Record/529930021/Details#tabnav)) |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------|
| &lt;mods:title>**Cahiers de la Méditerranée**</mods:title>                                                                                                         | 773 $t **Cahiers de la Méditerranée**                                                      |
| &lt;mods:number>**95**</mods:number>                                                                                                                               | 773 $g **95**(2017-12-31), **329-331**                                                     |
| &lt;mods:extent unit="pages">&lt;mods:list>**329-331**</mods:list></mods:extent>                                                                                   |                                                                                            |

---

### Interpretation und Ungenauigkeit
* Bei Umwandlung von weniger strukturierten Daten in stark strukturierte Daten
* Daten müssen interpretiert werden, um sie zu übertragen 

---

| dc:date | zu  | MARC 264                      |
|---------|-----|-------------------------------|
|         |     | 264 _0 $c = Entstehungsdatum  |
|         |     | 264 _1 $c = Erscheinungsdatum |
| date    |     | 264 _2 $c = Vertriebsdatum    |
|         |     | 264 _3 $c = Herstellungsdatum |
|         |     | 264 _4 $c = Copyrightdatum    |

---

### Datenqualität
* Datenqualität des Datenbestands mit dem zusammengeführt werden soll ist nicht oder nur mit viel Aufwand zu erreichen
* Elemente müssen hinzugefügt werden oder fehlen
* Beispiel: fehlende Formatangaben für MARC-Daten (IMD-Typen, Codierung)

---

### Datenqualität
* Datenset enthält nicht dem Standard entsprechende Daten
* Automatisierte Transformation funktioniert nicht oder muss angepasst werden
* In grossen Datenmengen sind Fehler, die nur wenige Daten betreffen, leicht zu übersehen
* Gründe: Fehler bei der Erfassung, Altdaten, Kontext, ...

---

BORIS Sprachcode "me" für mehrsprachig
* 2stellig, andere Codes sind 3stellig
* nach ISO 639-3 wäre "mul" zu verwenden

{{% /section %}}




