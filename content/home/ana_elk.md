+++
weight = 410
draft = true
+++

## ELK

Datenvisualisierung basierend auf Suchserver

Komponenten:
* **E**lasticsearch
* **L**ogstash
* **K**ibana
* Beat-Applikationen (spezifisch nach Anwendungsfall)

---

#### Elasticsearch
* Dokumentenorientierte Datenbank
* Kein festgelegtes Schema
* Speichert im JSON-Format
* Suchmaschine
* Index-basiert Aggregationen möglich

---

#### Kibana
* Exploration via Suchabfrage auf Index
* Visualisierung mit vordefinierten Diagrammen
* Dashboards als interaktive Reports

---

### Einsatzbereiche
* Monitoring
* Nutzungsstatistik
* Analyse Metadaten