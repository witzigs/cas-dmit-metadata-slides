+++
weight = 310
+++
{{% section %}}
## OAI-PMH

---

### Intro
* **O**pen **A**rchives **I**nitiative **P**rotocol for **M**etadata **H**arvesting
* Protokoll zum Sammeln von Metadaten, [Spezifikationen](https://www.openarchives.org/OAI/openarchivesprotocol.html)
* 2001, Version 1.0 ; 2002, Version 2.0
* Hintergrund Entwicklung: Austausch und Interoperabilität von (institutionellen) Repositories / preprint-Repositories, etc.
* Eingesetzt in OA-Repositories, aber auch von Bibliotheken, Museen, Archiven, etc.

---
### Anwendungsbeispiele
* Aggregatoren wie [Europeana](https://www.europeana.eu/de), [swisscollections](https://swisscollections.ch/), [Base](https://www.base-search.net/), ...
* Anbindung GND an Community Zone von Alma

---

### Grundprinzipien
* Data Provider: stellt Daten über OAI-PMH zur Verfügung
* Service Provider: harvestet Daten über OAI-PMH und verwendet sie für einen Service
* Basiert auf XML/HTTP

---

### Grundprinzipien
* Resource = die Publikation selbst
* Item = die Publikation im Repository
* Record = die Metadaten in einem bestimmten Format

---

### Vorgaben für OAI-Schnittstellen
* Metadaten in XML ausgeliefert, mindestens in Dublin Core (mehr Formate möglich)
* Identifier für Item, zwingend unique im Repository
* Umgang mit gelöschten Daten festgelegt (3 Optionen)
* Datestamps für selektives Harvesting
* Optional Sets für selektives Harvesting
* Unterstützung der 6 definierten Requests

---

### Record

[Beispiel](https://www.zora.uzh.ch/cgi/oai2?verb=GetRecord&metadataPrefix=oai_dc&identifier=oai:www.zora.uzh.ch:2951)

header:
* identifier (Scheme, Repository, Systemnummer)
* datestamp (letzte Aktualisierung)
* sets

metadata:
* Metadaten im gewählten Format

---

### [Requests und Arguments](https://witzigs.gitlab.io/cas-dmit-metadata/daten_beziehen/oai/requests/)

* Daten erhalten
  * GetRecord: ruft einen einzelnen Record auf
  * ListRecords: ruft eine Liste aller Records auf
  * ListIdentifiers: ruft eine Liste aller Identifier auf
* Informationen zum Repository
  * Identify: ruft Informationen über das Repository auf
  * ListMetadataFormats: ruft eine Liste der verfügbaren Metadatenformate auf
  * ListSets: ruft eine Liste der verfügbaren Sets auf

---

### [Übung zu OAI-PMH](https://witzigs.gitlab.io/cas-dmit-metadata/daten_beziehen/oai/uebung/)


{{% /section %}}