+++
weight = 240
+++

{{% section %}}
### BIBFRAME

---

#### Intro
* **Bib**liographic **Frame**work Initiative
* Library of Congress
* Ziele: Ablösung von MARC, dabei robustes Austauschformat erhalten
* Diverse Aspekte einbezogen: Datenaustausch, auch ausserhalb der Bibliotheken, Tools und Prozesse für eine Umstellung von MARC zu BIBFRAME, Methoden für die Erschliessung, ...


<img class="special-img-class" style="width:15%;border:none" src="/images/bibframe-logo-small.jpeg" />

---

#### Intro
* Datenmodell und Vokabular nach RDF
* 2012 Veröffentlichtung BIBFRAME 1.0, 2016 überarbeitet zu BIBFRAME 2.0
* Entwicklung und Einsatz in einzelnen Bibliotheken
  * Library of Congress
  * Libris (Schweden)
  * RERO+
  * ...
* Entwicklung von Editoren, Mappings und Skripts für Transformation

---

#### Intro

* Ergänzung des Vokabulars
* Kontinuierliche Entwicklung Vokabular über [GitHub](https://github.com/lcnetdev/bibframe-ontology)
* BIBFRAME Interoperability Group ([BIG](https://wiki.lyrasis.org/pages/viewpage.action?pageId=249135298)) seit 2022

---

#### [BIBFRAME Modell](https://www.loc.gov/bibframe/docs/bibframe2-model.html)

![Bibframe model](/images/bf2-model.jpeg "image")

---

### BIBFRAME Vocabulary
[BIBFRAME Ontology: List view](https://id.loc.gov/ontologies/bibframe.html)


<!--

---

### BIBFRAME in der Praxis
**Library of Congress**

* Pilotphase Erschliessung mit BIBFRAME (2017-2020)
* ab 2021 langsamer Umstieg auf BIBFRAME für die Katalogisierung
* Entwicklung von Mappings, Workflows und [Extensions](https://id.loc.gov/ontologies/bflc.html)
* Entwicklung Transformationen ([MARC zu BIBFRAME](https://www.loc.gov/bibframe/mtbf/), aber auch [BIBFRAME zu MARC](https://www.loc.gov/bibframe/bftm/))
* Entwicklung [Editor](http://bibframe.org/bfe/index.html)
* Publikation von [Datensets](https://id.loc.gov/)

---

### BIBFRAME in der Praxis
**Libris (Schweden)**

* Erschliessung in auf BIBFRAME basiertem Vokabular seit 2018
* Entwicklung [Datenmodell](https://id.kb.se/vocab/), "extending BIBFRAME"
* Entwicklung von Transformationen (MARC zu LD und LD zu MARC)
* Entwicklung [Editor](https://libris.kb.se/katalogisering)
* Open Source ([GitHub](https://github.com/libris))

---

### BIBFRAME in der Praxis
**RERO+**

* Neues Bibliotheksverwaltungssystem [RERO ILS](https://www.rero.ch/de/produits/ils) seit 2021
* Eingesetzt bei 58 [Bibliotheken](https://www.rero.ch/de/about/clients/clients-rero-ils), mehrheitlich öffentliche Bibliotheken
* Erschliessung in BIBFRAME
* Editor, in [Demo-Umgebung](https://ils.test.rero.ch/) zum Ausprobieren
* Migration von MARC zu BIBFRAME mit eigenen Spezifikationen
* JSON, keine RDF-Serialisierung
* Oberfläche ([Beispiel](https://bib.rero.ch/global/documents/1011252)) und API mit JSON-Daten ([Beispiel](https://bib.rero.ch/api/documents/1011252))
* SRU mit MARC-Daten ([Beispiel](https://bib.rero.ch/api/sru/documents?version=1.1&operation=searchRetrieve&query=pid=1011252))

---

### BIBFRAME in der Praxis

**Ex Libris**

* MARC zu BIBFRAME Transformation in Alma integriert für Export
* APIs für den Export von BIBFRAME, vgl. [Alma FAQ](https://knowledge.exlibrisgroup.com/Alma/Product_Materials/050Alma_FAQs/General/Standards#BIBFRAME) und [Developer Network](https://developers.exlibrisgroup.com/alma/integrations/linked_data/bibframe/)
* Erschliessung via API geplant für 2024, vgl. [Alma Linked Open Data Roadmap 2024](https://knowledge.exlibrisgroup.com/Alma/Product_Materials/010Roadmap/Alma_Roadmap_Highlights_(2024-2025)/Linked_Data)

---

### Linked Data und Bibliotheken: Fragen über Fragen
* Welcher Fokus? Erschliessung als Linked Data oder Publishing?
* Welcher Standard für die Erschliessung? BIBFRAME mit RDA? Lokale Ergänzungen?
* Welcher Standard für das Publishing? BIBFRAME? schema.org? Lokale Datenmodelle?
* Wie umgehen mit legacy data?
* Wie umgehen mit der Abhängigkeit von Systemen, Workflows, ... zu MARC?

-->

{{% /section %}}

---

#### Linked Data Vokabulare

* Schon erwähnt: [BIBFRAME](https://id.loc.gov/ontologies/bibframe.html), [MADS/RDF](https://id.loc.gov/ontologies/madsrdf/v1.html), [DCMI Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)
* Bibliotheksbereich: [RDA Element sets](http://www.rdaregistry.info/), [GND Ontology](https://d-nb.info/standards/elementset/gnd#)
* Archivbereich: [Records in Context Ontology RiC-O](https://ica-egad.github.io/RiC-O/index.html)
* Weitere:  [Wikidata](https://www.wikidata.org/wiki/Wikidata:List_of_properties), [FOAF](http://xmlns.com/foaf/0.1/), [Schema.org](https://schema.org/), [PROV-O](https://www.w3.org/TR/prov-o/), [PREMIS](http://www.loc.gov/standards/premis/ontology/index.html), [CIDOC CRM](http://www.cidoc-crm.org/), [SKOS](https://www.w3.org/2009/08/skos-reference/skos.html), ...