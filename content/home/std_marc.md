+++
weight = 210
+++
{{% section %}}
### MARC

---

#### Intro
* **Ma**chine **R**eadable **C**ataloging
* Aktuell: MARC21
* Library of Congress
* DER Standard im Bibliotheksbereich (trotz "MARC must die"...)
* diverse Varianten

<img class="special-img-class" style="width:15%;border:none" src="/images/marc.png" />

---

#### Geschichte und Entwicklung
* Ab 1966 entwickelt
* LoC, Henriette Avram
* Metadaten maschinenlesbar machen
* Abläufe und Austausch automatisieren
* Katalogkarten und ISBD Interpunktion

---

#### Versionen
* MARC21
* UNIMARC -> IFLA, Frankreich, div. weitere (auch Basis für weitere Versionen)
* Land/Sprachspezifisch (z.B.: CNMARC -> China)
* IDSMARC -> Deutschschweiz, mit SLSP grösstenteils abgelöst durch MARC21
* ...

---

#### MARC21 - 5 Standards
* Bibliographic
* Authority
* Holdings
* Classifications
* Community

---

#### Bibliographic - Struktur
* Leader
* Kontrollfelder
* Datenfelder

---
##### Leader
* Enthält Verarbeitungsinformationen zum Datensatz
* 24 Zeichen
* Keine Indikatoren und Unterfelder
* Elemente sind positionsabhängig

![Leader](/images/marc-leader.png)

---
##### Kontrollfelder
* Feldbereich 00x
* Enthalten codierte Informationen
* Teilweise fixe Länge und positionsabhängig
* Keine Indikatoren und Unterfelder

![Kontrollfelder](/images/marc-controlfields.png)

---

##### Datenfelder
* 3-stellige Feldnummer
* 2 numerische Indikatoren
* Variable Unterfelder
* Wiederholbarkeit (Felder und Unterfelder)

<img class="special-img-class" style="width:80%;border:none" src="/images/marc-fields.png" />

---

##### Datenfelder - Gruppierung
* **01X-09X**: Nummern und Codes
* **1XX**: Haupteintragungen
* **20X-24X**: Titel- und damit verbundene Felder
* **25X-28X**: Ausgabe- und Impressumsfelder
* **3XX**: Felder für die physische Beschreibung
* **4XX**: Gesamttitelangaben
* **5XX**: Fussnoten
* **6XX**: Schlagworteintragungen
* **70X-75X**: Nebeneintragungen
* **76X-78X**: Verknüpfungseintragungen
* **80X-83X**: Nebeneintragungen Gesamttitel
* **841-88X**: Bestandsdaten, andere Schriften etc.

---

##### Orientierungshilfen

* Bibliographische Beschreibung
* Eintragungen
    * Personen, Körperschaften, Konferenzen, Werke, Orte, (Themen)
    * die gleichen letzten zwei Zahlen in Feldnummer
    * Unterfelder gleich verwendet
* Durchgehend identisch verwendete Unterfelder
    * $2 für Quelle
    * $0 für Identifier

---

#### Lokale Datenelemente
* **9XX**: Lokale Felder
* Auch in x9x, xx9
* Unterfeld $9

---

#### Dokumentation
* [MARC Standards](https://www.loc.gov/marc/)
* [MARC21 Bibliographic](https://www.loc.gov/marc/bibliographic/)
* Lokale Hilfsmittel (Anleitungen, Feldhilfen, etc.)

---

#### Verwendete Austauschstandards
* MARCXML
* MARC Bandformat (ISO-2709)
* Speicherformate sind systemabhängig

---

##### MARC ISO-2709

![Bandformat](/images/marc-band.png)

--- 

##### MARCXML

[MARCXML Schema](https://www.loc.gov/standards/marcxml//)

<img class="special-img-class" style="width:55%" src="/images/marc-xml.png" />


<!--
##### Zeilenbasierte Arbeitsformate

![Aleph seq](/images/marc-seq.png)

---
-->

---

#### MARC21 Einordnung
* Primär Strukturstandard
* Auch Datenwertstandard (z.B. Sprachcodes)

---

#### Probleme
* Verständlichkeit Feldnummern
* Komplex zum Erlernen
* Sehr unterschiedlich anwendbar
* Redundanzen
* Ursprüngliche Ausrichtung auf physische Medien
* Entwicklung schwerfällig durch weite Verbreitung und Prozesse für Änderungen

[comment]: <> (* Verknüpfung zwischen Datensätzen: nachträglich eingeführt, initial mit Strings gedacht)
[comment]: <> (* Hauptansetzung, etc.)


{{% /section %}}