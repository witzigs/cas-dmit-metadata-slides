+++
weight = 600
+++

# Daten transformieren

---

## Ziele

* Daten verbessern
* Daten in verschiedenen Formen zur Verfügung stellen
* Daten aus verschiedenen Quellen aggregieren und homogenisieren
* Daten indexieren
* Daten für Weiterverarbeitung und Analyse aufbereiten

---

## Vorgehen

* Anforderungen an die Ziel-Daten verstehen
* Falls schon vorhanden, Daten im Zielsystem analysieren
* Quell-Daten analysieren
* Entscheidungen zur Datenmodellierung treffen
* Detailliertes Mapping erstellen

---

## Hilfsmittel

* Crosswalks zwischen Formaten
  * Beispiel: [MARC21 zu MODS 3.7](https://www.loc.gov/standards/mods/v3/mods-mapping-3-7.html)
* Zur Verfügung gestellte Skripte
  * Beispiel: [MARC21 zu BIBFRAME XSLT Skripte](https://github.com/lcnetdev/bibframe2marc)
* Vorhandene Mappings für die gleichen Daten

---

## Mapping

* Quell- und Zielfelder eindeutig identifizieren
* Kriterien als Pseudocode formuliert
* Ergänzung von Datenelementen
* Berücksichtigung von Wiederholbarkeit
* Berücksichtigung von Trennzeichen
* Berücksichtigung von verschachtelten Datenstrukturen
* Möglichst alle in den Quell-Daten vorkommende Fälle berücksichtigen