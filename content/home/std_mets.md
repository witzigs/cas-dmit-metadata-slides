+++
weight = 270
+++
{{% section %}}

### METS

---

#### Intro
* **M**etadata **E**ncoding and **T**ransmission **S**tandard
* Bietet einen Rahmen um deskriptive, administrative und strukturelle Metadaten in ein XML-Dokument abzubilden
* Strukturierung von digitalen Objekten
* Austauschformat für digitale Objekte
* Digitale Sammlungen (e-rara, e-manuscripta)
* Digitale Langzeitarchivierung

<img class="special-img-class" style="width:40%;border:none" src="/images/mets.gif" />

---

#### Daten in METS
* Hierarchische Struktur von digitalen Objekten
* Name und Links dieser Objekte
* Deskriptive Metadaten können im Dokument sein oder es kann darauf verwiesen werden

---

#### Struktur
* METS Header: Metadaten über das METS Dokument
* Descriptive Metadata: Deskriptive Metadaten (häufig MODS, aber andere Formate möglich)
* Administrative Metadata: technische Metadaten, Angaben zur Herkunft, Urheberrechte ...
* File Section: Dateien, die zum digitalen Objekte gehören

---

#### Struktur
* Structural Map: hierarchische Beschreibung der Struktur des digitalen Objektes
* Structural Links: Links innerhalb des Objekts
* Behaviors: Informationen für ausführbare Elemente (Software, die benötigt wird um mit dem Objekt oder den Metadaten zu arbeiten)

---

#### Dokumentation und Beispiele
* [METS Official Web Site](http://www.loc.gov/standards/mets/)
* [Beispiel aus e-rara (Oberfläche)](https://www.e-rara.ch/zut/content/titleinfo/198)
* [Beispiel aus e-rara (OAI)](https://www.e-rara.ch/oai/?verb=GetRecord&metadataPrefix=mets&identifier=oai:www.e-rara.ch:198)

{{% /section %}}