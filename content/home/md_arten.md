+++
weight = 101
+++

## Grundlagen

---

{{% section %}}

### Datenmodell

* Ausschnitt der Welt
* Bestimmter Anwendungszweck

---

### Datenmodellierung

* Welche Entitäten stehen im Zentrum?
* Wie sind sie miteinander verknüpft?
* Welche ihrer Eigenschaften werden beschrieben?
* Was wird nicht beschrieben?
* Wie differenziert wird beschrieben?

---

#### Datenmodellierung im Bibliotheksbereich

Publikationen im weitesten Sinne:

* Bücher, Zeitschriften, Noten, Karten, audiovisuelle Medien, ...
* Physische oder elektronische Ressourcen
* Publizierte oder graue Literatur

---

#### Datenmodellierung im Bibliotheksbereich
* Publikationen
    * Abstrakte Entität
    * Bibliographische Daten
* Instanzen von Publikationen
    * Physisch oder elektronisch
    * Exemplare
* Entitäten, die mit Publikationen in Beziehung stehen
    * Personen, Körperschaften, Orte, Sachbegriffe
    * Normdaten

{{% /section %}}

---

{{% section %}}

### Arten von Metadaten

* Deskriptive Metadaten
* Administrative Metadaten
* Strukturelle Metadaten

---

#### Deskriptive Metadaten
* Bibliografische Daten
* Finden und identifizieren

---

![Ente](/images/deskriptiv-ente.png)

---

#### Administrative Metadaten
* Daten zur Verwaltung von Ressourcen: Einkaufen, Ausleihen, Digitalisieren, ...
* Technische Metadaten
* Rechtliche Metadaten
* Metadaten zur Erhaltung

---

![Lizenzverwaltung](/images/lizenzverwaltung.png)

---

#### Strukturelle Metadaten

* Beziehungen von Teilen
* Strukturierung von Digitalisaten

---

![Struktur Digitalisat](/images/digitalisat-struktur.png)

{{% /section %}}

<!--
---

### Ihre Erfahrungen mit Metadaten

Kurzer Austausch mit einer Person nebendran über...
* ... die eigenen Erfahrungen mit Metadaten.
* ... die im Alltag genutzten Metadatenformate und Standards.
* ... die Erwartungen an den heutigen Vormittag.

-->