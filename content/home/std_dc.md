+++
weight = 220
+++

{{% section %}}
### Dublin Core

---
#### Intro
* Dublin Core Metadata Element Set (DC, DCMES): 15 Elemente
* DCMI Metadata Terms (DC Terms): Element Set und zusätzliche weitere Elemente
* Beschreibung von Objekten im Internet
* Auch für physische Ressourcen verwendet
* Repositories, GLAM-Bereich, Software, ...

<img class="special-img-class" style="width:15%;border:none" src="/images/dublincore.png" />

---

#### Geschichte und Entwicklung
* Workshop 1995: Initiert von OCLC, unter Beteiligung von Personen aus Bibliothek, IT, Forschung
* Ziele:
    * Bessere Beschreibung von Ressourcen im Internet für Verbesserung der Auffindbarkeit
    * Anwendbar ohne Expertenwissen, damit möglichst viel beschrieben werden kann
* Element Set als Resultat
    
---

#### Struktur - Element Set

* 15 Elemente
* wichtigste Elemente für Auffindbarkeit
* interdisziplinär
* nutzbar für diverse Ressourcen (digital und physisch)
* einfach und flexibel (optional, wiederholbar, Reihenfolge irrelevant)

---

#### Struktur - Element Set

<div class="container">

<div class="col">
<ul>
<li>contributor</li>
<li>coverage</li>
<li>creator</li>
<li>date</li>
<li>description</li>
<li>format</li>
<li>identifier</li>
</ul>

</div>

<div class="col">
<ul>
<li>language</li>
<li>publisher</li>
<li>relation</li>
<li>rights</li>
<li>source</li>
<li>subject</li>
<li>title</li>
<li>type</li>
</ul>

</div>

</div>

---

#### Weiterentwicklung
* 2001 Simple DC und Qualified DC:
    * Elemente so breit definiert, dass tlw. unklar wird was gemeint ist (date)
    * Erweiterung mit qualifier (date -> date.created, date.modified, date.issued, ...)
* 2003 DCMI Metadata Terms
    * Publikation als RDF
    * qualifier als eigene Elemente (date, created, modified, issued, ...)

---
<!--
#### DCMI Metadata Terms Struktur
* n Elemente
* gewisse constraints
* Empfehlungen für Erfassung, Datenwertstandard (insbesondere mit DCMI Types als eigenem Vokabular)

---
-->

#### Dokumentation und Beispiele
* [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)
* [Beispiel DC Elements aus edoc](https://edoc.unibas.ch/cgi/oai2?verb=GetRecord&identifier=oai:edoc.unibas.ch:67215&metadataPrefix=oai_dc)

---

#### Einordnung
* Primär Strukturstandard
* Empfehlungen für die Verwendung von Datenwertstandards (DCMI Types, ISO 639-2 für Sprache, ...)
* Austauschstandards: RDF-Serialisierungen, XML
* Sehr einfach, sehr verbreitet genutzt


{{% /section %}}

