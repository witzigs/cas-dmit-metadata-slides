+++
weight = 330
+++

### OAI oder SRU?

| OAI                                               | SRU                                                         |
|---------------------------------------------------|-------------------------------------------------------------|
| Harvesting, einmalig oder regelmässig             | Suche in externer Quelle und Darstellung in eigenem Kontext |
| Abholen von Updates                               | Adhoc Abfrage, die immer alle Daten zurückliefert           |
| Rudimentäre Selektionskriterien (sets, datestamp) | Ausgefeilte Suchmöglichkeiten (CQL)                         |

---

### Z39.50
* Netzwerkprotokoll zur Abfrage von Bibliothekssystemen (nicht HTTP!)
* Nur im Bibliothekswesen genutzt
* Entwickelt ab ca. 1980
* Vorgänger von SRU
* Nutzung wie SRU, Suchabfragen auf Server (z.B. Suche in Fremddaten)
* Beispiel: [Alma Z39.50](https://developers.exlibrisgroup.com/alma/integrations/z39-50/)

---

### Systemspezifische Schnittstellen
* Schnittstellen basierend auf Prinzipien von REST, WebHooks, ...
* Nur innerhalb eines Systems standardisiert
* Können auch schreibenden Zugriff ermöglichen
* Ermöglichen Automatisierung von Aufgaben, Anbindung von Drittsystemen, Verändern von Daten, ...
* Beispiel: [Alma REST APIs](https://developers.exlibrisgroup.com/alma/apis/)
* Beispiel: [lobid GND API](https://lobid.org/gnd/api)

---

### SPARQL
* Abfragesprache für RDF-Daten
* Beispiel: [Wikidata Query Service](https://query.wikidata.org/)

---

### Gibt es eine Schnittstelle?
* Schnittstellen sind im Funktionsumfang des Systems enthalten oder nicht
* Dokumentation des Systems oder des Service
* Nachfragen
* Entwicklung anstossen
* Evtl. Eigenentwicklung (eher Kontext Service als System)

---

### Daten über Schnittstellen beziehen
* Skripte, z.B. in Python
    * [Jupyter Notebook Tutorials der DNB](https://www.dnb.de/DE/Professionell/Services/WissenschaftundForschung/DNBLab/dnblabTutorials.html?nn=849628#doc1038762bodyText2)
    * [Python Digital Toolbox Jupyter Notebooks der UB Bern](https://github.com/ub-unibe-ch/ds-pytools)
    * [Jupyter Notebooks der ZB](https://data.zb.uzh.ch/map/books/data-map-der-zentralbibliothek-zurich/page/jupyter-notebooks-der-zentralbibliothek-zurich)
* Software mit enthaltenem OAI- und/oder SRU-Client (z.B. [Catmandu](https://librecat.org/index.html), [MarcEdit](https://marcedit.reeset.net/), ...)


---

{{% section %}}

## Datendump

---

### Datendump
* Einfachste Möglichkeit für Datenaustausch
* Geeignet für einmaligen Datenaustausch
* Weniger Programmierkenntnisse zum Abholen und zur Verfügung stellen
* Umgang mit grossen Datenmengen
* Handarbeit
* MARCXML, Linked Data
* CSV, JSON, XML, ...

{{% /section %}}

---

{{% section %}}

## Datenangebote für Forschende

---

### Hintergrund
* Bedürfnis Forschungsobjekte als Daten zu beziehen
* Individuelle Korpusbildung
* [Forschungdaten aus dem Bibliothekskatalog, ZB Lab](https://www.zb.uzh.ch/de/news/zblog/zblog-2024#forschungsdaten-aus-dem-bibliothekskatalog)

---

### Hintergrund
* Metadaten und Digitalisate interessant
* Individuelle Datenselektion ermöglichen (detaillierter als OAI, einfacher als SRU...)
* Einfacherer Zugang als über Schnittstellen/Dump schaffen

---

#### Beispiel: swisscollections Datenexport
* [swisscollections](https://swisscollections.ch/): Sucheinstieg zu historischen und modernen Sammlungen in Schweizer Bibliotheken und Archiven
* Historische Bestände, Digitalisate unter freier Lizenz
* Suchoberfläche basierend auf VuFind und Solr (Open Source)
* Verein swisscollections, ZB-Lab
* Entwicklungsteam UB IT und Externe

---

#### Beispiel: swisscollections Datenexport
* Export eines Metadatenpakets integriert als Service in die Suchoberfläche
* Metadaten als CSV, JSON, MARCXML
  * CSV und JSON: Format möglichst nah an der Darstellung auf swisscollections
* Links auf Digitalisate bei e-rara und e-manuscripta
  * HTML-Seiten: für Download via Downloadmanager
  * CSV und JSON enthalten
* [Dokumentation](https://ub-basel.atlassian.net/wiki/x/BIA2lg)

---

#### Beispiel: Jupyter Notebooks ZB
* [Jupyter Notebooks ZB](https://data.zb.uzh.ch/map/books/data-map-der-zentralbibliothek-zurich/page/jupyter-notebooks-der-zentralbibliothek-zurich)
* Nutzt SRU-Schnittstelle von SLSP (Alma)
* Vereinfacht Nutzung der SRU-Schnittstelle
* Export von bis zu 10'000 Aufnahmen in Excel
* Datenanalyse vorbereitet


{{% /section %}}