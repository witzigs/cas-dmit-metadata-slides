+++
weight = 500
draft = true
+++

# Metadaten als Linked Data

---

{{% section %}}
## RDF

---

### Intro
* **R**esource **D**escription **F**ramework
* Zentraler Baustein für Linked Data
* Modell für die Beschreibung von Informationen als Aussagen
* Abstraktes Modell mit diversen Serialisierungen (RDF/XML, Turtle, N-Triples, JSON-LD)


---

### Triple (Aussagen)
Ein Triple ist eine Aussage, die ein Subjekt und ein Objekt miteinander in Beziehung setzt.

Die Beziehung ist vom Subjekt zum Objekt gerichtet und wird mit einem Prädikat benannt.

---
![Triple](/images/tolkien-triple.png "image")

---

### Ressourcen und Literale
* Ressourcen sind eindeutig identifizierbar und werden mit einer URI bezeichnet
* Subjekte und Prädikate sind immer Ressourcen
* Literale sind Zeichenketten (Text, Zahlen, ...)
* Objekte können Ressourcen oder Literale sein

---
![Triple](/images/tolkien-triple-uri.png "image")

---

![Triple](/images/tolkien-triple-literal.png "image")

---

### Vokabulare und Ontologien
* Definieren Klassen (Entitäten, Konzepte) und deren hierarchische Struktur
* Definieren Properties (Eigenschaften, Beziehungen) der Klassen
* Halten Definitionen und Constraints (Einschränkungen) fest
* Beispiel [DCMI Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)
* Beispiel RiC-O hasCreator

---

![Triple](/images/tolkien-class.png "image")

---

### Vokabulare und Ontologien

<img class="special-img-class" style="width:60% ; border:none" src="/images/LOV.png" />

[Linked Open Vocabularies](https://lov.linkeddata.es/dataset/lov/)

---

### Vokabulare und Ontologien

* Können von jedem erstellt werden
* Werden in RDF abgebildet
* Fachspezifisch, Domänenspezifisch, Universell
* Bibliotheksbereich: [RDA Element sets](http://www.rdaregistry.info/), [BIBFRAME](https://id.loc.gov/ontologies/bibframe.html), [MADS/RDF](https://id.loc.gov/ontologies/madsrdf/v1.html), [GND Ontology](https://d-nb.info/standards/elementset/gnd#)
* Archivbereich: [Records in Context Ontology RiC-O](https://ica-egad.github.io/RiC-O/index.html)
* Weitere: [DCMI Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/), [Wikidata](https://www.wikidata.org/wiki/Wikidata:List_of_properties), [FOAF](http://xmlns.com/foaf/0.1/), [Schema.org](https://schema.org/), [PROV-O](https://www.w3.org/TR/prov-o/), [PREMIS](http://www.loc.gov/standards/premis/ontology/index.html), [CIDOC CRM](http://www.cidoc-crm.org/), [SKOS](https://www.w3.org/2009/08/skos-reference/skos.html), ...

---

### Datensets

<img class="special-img-class" style="width:50% ; border:none" src="/images/lod-cloud.jpeg" />

[Linked Open Data Cloud](https://lod-cloud.net/)


---

### Datensets

* Bibliotheken: Bibliografische Daten, Autoritätsdaten (z.B. GND)
* Beispiel: [lobid](https://lobid.org/)
* Allgemein: [Wikidata](https://www.wikidata.org), [DBpedia](https://www.dbpedia.org/), [GeoNames](https://www.geonames.org/) und viele, viele weitere!

---

### Daten beziehen

**Schnittstellen**
* je nach Service unterschiedlich
* Beispiel: [Alma JSONLD API](https://developers.exlibrisgroup.com/alma/integrations/linked_data/jsonld/)
* Beispiel: [lobid GND API](https://lobid.org/gnd/api)

**SPARQL-Endpunkte**
* Abfragesprache für RDF-Daten
* Beispiel: [Wikidata Query Service](https://query.wikidata.org/)

{{% /section %}}