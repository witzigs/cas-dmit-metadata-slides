+++
weight = 209
+++

### Datenstrukturstandards
* MARC
* Dublin Core
* MODS
* BIBFRAME
* METS

---

### Beispiele vergleichen - Leitfragen

* Was fällt auf?
* Was sind Unterschiede?
* Was ist gleich?
* Datenstrukturstandard, Dateninhaltsstandard, Datenwertstandard