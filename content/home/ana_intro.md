+++
weight = 400
+++

# Daten analysieren

--- 

### Voraussetzung

* Schritt 1: Datenquellen identifizieren
* Schritt 2: Daten beziehen oder abholen
* Schritt 3: Daten kennenlernen und einschätzen

---

### Fragestellungen

* Wie ist die Datenqualität? 
* Ist die Datenqualität ausreichend für mein Projekt?
* Sind zwingend notwendige Elemente enthalten?    
* Kann ich selbst Verbesserungen an den Daten vornehmen?
* Kann ich die Informationen, die ich brauche, extrahieren?
* Brauche ich zusätzliche Dokumentation oder Informationen von Ansprechpartner*innen?
* Sind meine Annahmen korrekt?
* ...

---

### [Tools](https://witzigs.gitlab.io/cas-dmit-metadata/daten_transformieren/tools/)

* Shellbefehle (grep, sort, uniq, cut ...)
* Excel für CSV-Dateien
* MarcEdit für MARC-Daten
* OpenRefine
* Catmandu
* Elastic Search und Kibana
* Skripte in diversen Sprachen
* ...

---

{{% section %}}


### CSV

* **C**omma **S**eparated **V**alues
* Textdatei mit einfach strukturierten Daten
* Zum Austausch von Daten, die in Tabellen vorliegen verwendet

--- 

#### Elemente

* Zeichen zur Trennung von Datensätzen (Zeilen): In der Regel Zeilenumbruch
* Zeichen zur Trennung von Datenfeldern (Spalten): Häufig Komma, kann aber auch Tabulator, Semikolon, Doppelpunkt, ... sein
* Zeichen zur Begrenzung von Datenfeldern: In der Regel doppeltes Anführungszeichen (")

---

#### Bearbeitung

* Texteditor
* Tabellenkalkulationsprogramme: Excel, LibreOffice Calc, ...

---

#### Stolpersteine mit Excel & Co.

* Zeichensatz: beim Import in Excel prüfen, dass Umlaute und andere Sonderzeichen korrekt dargestellt werden
* Trennzeichen: korrektes Trennzeichen wählen und prüfen, dass alle Zeilen in der gleichen Spalte enden
* Excel-Feldformate: interpretieren und verändern Daten!

---

##### Veränderte Daten

[Research Data Scarytales, FDM Thüringen](https://forschungsdaten-thueringen.de/geschichten.html)

[Geschichte 02 Seltsam - Preise haben Geburtstag](https://forschungsdaten-thueringen.de/geschichten/articles/counter-de.html)


Beim Versuch eine Kostenanalyse zu erstellen, war die zuständige Mitarbeiterin überrascht, als in ihrer Tabelle immer wieder abwegige Ergebnisse am Ende ihrer Berechnungen standen. Nach kurzer Recherche war klar, dass beim Import der CSV-Datei für die Nutzungsstatistik elektronischer Medien in Excel aus den Preisen automatisch Datumsangaben gemacht wurden.

{{% /section %}}

---

### Übung
* Analyse eines [Datensets "Mikrofilmarchiv Musik"](https://witzigs.gitlab.io/cas-dmit-metadata/daten_analysieren/uebung-mikrofilmarchiv/) (XML, Dublin Core)
* Vorschlag Bearbeitung: mit Excel oder LibreOffice Calc