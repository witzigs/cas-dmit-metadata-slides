+++
weight = 620
draft = true
+++
{{% section %}}

## Datentransformation: Beispiel linked swissbib

---

### Kontext
* Projekt 2014-2017
* Zusammenarbeit UB Basel, HTW Chur, HEG Genève, GESIS
* Daten transformieren
* Daten zur Nachnutzung anbieten
* Daten zur Verbesserung der Suchoberfläche nutzen
* Produktiver Betrieb
* Open Source

---

### linked swissbib: MARC zu Linked Data

* Konversion der swissbib MARC-Daten in ein RDF-Datenmodell
* Datenmodellierung
* Entwicklung Datentransformation

---

#### Entwicklung Datenmodell
Benutzergesteuerte Entwicklung des Modells:

Was wollen wir auf der Oberfläche anbieten?

* Aggregationsseiten (eigener und angereicherter Inhalt)
* Knowledge Cards

&#8594; jeweils zu Autoren, Werken und Themen

---

#### Entitäten im Datenmodell
6 bibliographische Konzepte

* Bibliographic Resource
* Document
* Item
* Work
* Person
* Organisation

[linked swissbib Datenmodell](https://linked-swissbib.github.io/datamodel/)

---

#### Vokabularien in linked.swissbib

* Dublin Core (dc/dct)
* Bibliographic Ontology (bibo)
* Bibframe (bf)
* RDA – unconstrained properties (rdau)
* Friend of a friend (foaf)
* DBpedia (dbo)
* Schema (sch)
* GND (gnd)
* Wikidata (wdt)
* Web Ontology Language (owl)
* RDF Schema (rdfs)
* Simple Knowledge Organisation System (skos)

---

#### MARC zu RDF
**Bibliographic Resource**
* Die am häufigsten vorkommenden MARC Felder werden transformiert
* Für Oberfläche notwendige MARC Felder werden transformiert
* Enthält Informationen aus 22 MARC-Feldern

**Person und Corporate**
* Extrahiert aus 1xx/7xx
* Wenn Identifier (GND, RERO) vorhanden: Anreicherung

---

##### Input: MARC

<img class="special-img-class" style="width:45%;border: none" src="/images/linked-sb/bsp-jane-letters.png"  alt="MARC"/>

---

##### RDF Bibliographic Resource

<img class="special-img-class" style="width:95%;border: none" src="/images/linked-sb/bibres-jane-letters.png"  alt="Bibliographic Resource"/>

---

##### RDF Person

<img class="special-img-class" style="width:95%;border: none" src="/images/linked-sb/person-jane-sb.png"  alt="Person"/>

---

##### RDF alle Entitäten

<img class="special-img-class" style="width:90%;border: none" src="/images/linked-sb/bibresource-jane-letters_nur-sb.png"  alt="Alle Entitäten"/>

---

##### Input Anreicherung

<img class="special-img-class" style="width:95%;border: none" src="/images/linked-sb/externe-quellen_new-linked.png"  alt="Input Anreicherung"/>

---

##### Input Anreicherung

<img class="special-img-class" style="width:95%;border: none" src="/images/linked-sb/id-hub.png"  alt="Input Anreicherung IDs"/>

---
##### ID Hub

<img class="special-img-class" style="width:95%;border: none" src="/images/linked-sb/id-hub_sbperson.png"  alt="ID Hub"/>

---
##### RDF Person angereichert

<img class="special-img-class" style="width:95%;border: none" src="/images/linked-sb/person-anreicherung_new-linked.png"  alt="Person angereichert"/>

---

#### Oberfläche und Schnittstelle

<div class="container">

<div class="col">
<img class="special-img-class" style="width:100%;border:none" src="/images/linked-sb/jane-knowledge-card.png"  alt="Knowledge Card"/>
</div>

<div class="col">
<img class="special-img-class" style="width:100%;border:none" src="/images/linked-sb/data-api.png"  alt="API"/>
</div>

</div>

{{% /section %}}

---

